![Logo](./images/local/k3s-logo.png)

K3s est une distribution Kubernetes très légère (cinq "s" de moins que dans K8s 😉).
C'est une distribution certifiée, très adaptée pour l'IoT, l'Edge computing, ...

Sur Windows, MacOS ou Linux, K3s peut facilement être installé dans une machine virtuelle. Nous utiliserons [Multipass](https://multipass.run) un outils très pratique qui permet de lancer facilement des machines virtuelles Ubuntu sur Mac, Linux, ou Windows. En fonction de l'OS, Multipass pourra utiliser un hyperviseur parmi Hyper-V, HyperKit, KVM, ou VirtualBox de manière native afin d'optimiser le temps de démarrage.

### 1. Installation de Multipass

Vous trouverez sur le site [https://multipass.run](https://multipass.run) la procédure d'installation de Multipass en fonction de votre OS ainsi que les commandes de bases (nous utiliserons certaines d'entre elles dans la suite).

![Multipass](./images/local/multipass.png)

### 2. Création d'une VM Ubuntu

Utilisez la commande suivante pour créer une VM Ubuntu 18.04 avec Multipass, cela ne prendra que quelques dizaines de secondes :

```
$ multipass launch --name k3s
```

Utilisez la commande suivante pour récupérer l'adresse IP de la VM qui vient d'être créée :

```sh
$ IP=$(multipass info k3s | grep IP | awk '{print $2}')
```

### 3. Installation de k3s

Utilisez la commande suivante pour lancer l'installation de k3s dans la VM que vous venez de provisionner :

```sh
$ multipass exec k3s -- bash -c "curl -sfL https://get.k3s.io | sh -"
```

### 4. Fichier de configuration

Récupérez, sur votre machine locale, le fichier de configuration généré par k3s :

```sh
$ multipass exec k3s sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml
```

Dans ce fichier, il est nécessaire de remplacer l'adresse IP locale (127.0.0.1) par l'adresse IP de la VM:

```sh
$ sed -i '' "s/127.0.0.1/$IP/" k3s.yaml
```

Utilisez ensuite la commande suivante afin de setter la variable d'environnement KUBECONFIG de façon à ce qu'elle pointe vers le path absolu du fichier de configuration récupéré précédemment :

```
$ export KUBECONFIG=$PWD/k3s.yaml
```

### 5. Test

Le cluster est maintenant prêt à être utilisé :

```
$ kubectl get nodes
NAME   STATUS   ROLES    AGE     VERSION
k3s    Ready    master   2m40s   v1.16.3-k3s.2
```

## Cluster multi-nodes

Le script suivant vous permet de créer un cluster k3s composé de 3 nodes :

```sh
for node in node1 node2 node3;do
  multipass launch -n $node
done

# Init cluster on node1
multipass exec node1 -- bash -c "curl -sfL https://get.k3s.io | sh -"

# Get node1's IP
IP=$(multipass info node1 | grep IPv4 | awk '{print $2}')

# Get Token used to join nodes
TOKEN=$(multipass exec node1 sudo cat /var/lib/rancher/k3s/server/node-token)

# Join node2
multipass exec node2 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Join node3
multipass exec node3 -- \
bash -c "curl -sfL https://get.k3s.io | K3S_URL=\"https://$IP:6443\" K3S_TOKEN=\"$TOKEN\" sh -"

# Get cluster's configuration
multipass exec node1 sudo cat /etc/rancher/k3s/k3s.yaml > k3s.yaml

# Set node1's external IP in the configuration file
sed -i '' "s/127.0.0.1/$IP/" k3s.yaml

# We'r all set
echo
echo "K3s cluster is ready !"
echo
echo "Run the following command to set the current context:"
echo "$ export KUBECONFIG=$PWD/k3s.yaml"
echo
echo "and start to use the cluster:"
echo  "$ kubectl get nodes"
echo
```
